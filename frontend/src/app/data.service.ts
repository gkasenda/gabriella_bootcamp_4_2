import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

    private courseList : Object[] = [
    {"id":"1", "course":"a"},
    {"id":"2", "course":"b"},
    {"id":"3", "course":"c"},}
  ];

  constructor() { }

   getCourseList():object[]
  {
    return this.courseList;
  }

}
