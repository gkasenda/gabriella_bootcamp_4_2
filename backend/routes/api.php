<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'mahasiswa'], function(){
Route::get('mahasiswaController@getData');
Route::post('mahasiswaController@addData');
Route::post('mahasiswaController@deleteData');
});

Route::group(['prefix' => 'course'], function(){
Route::get('courseController@getData');
Route::post('courseController@addData');
Route::post('courseController@deleteData');
});

Route::group(['prefix' => 'assignment'], function(){
Route::get('assignmentController@getData');
Route::post('assignmentController@addData');
Route::post('assignmentController@deleteData');
});

Route::group(['prefix' => 'grade'], function(){
Route::get('gradeController@getData');
Route::post('gradeController@addData');
Route::post('gradeController@deleteData');
});