<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use App\mahasiswa;

class mahasiswaController extends Controller
{
    function getData(){

        $mahasiswa = mahasiswa::get();

        return response()->json($userList, 200);
    }

    function addData(Request $request){
        DB::beginTransaction();
        try{

            $this->validate($request, [
                'name' => 'required',
            ]);

            $student_number = $request->input('student_number');
            $student_name = $request->input('student_name');
            $student_phonenumber = $request->input('student_phonenumber');
            $student_email = $request->input('student_email');

        }
    }

    $mhsw = new mahasiswas;
            $mhsw->student_number = $student_number;
            $mhsw->student_name = $student_name;
            $mhsw->student_phonenumber = $student_phonenumber;
            $mhsw->student_email = $student_email;
            $mhsw->save();

            DB::commit();
            return response()->json(["message" => "Success !!"], 200);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
        }

    }

    function saveData(Request $request){
        DB::beginTransaction();
        try{

            $this->validate($request, [
                'id' => 'required',
            ]);

            $id = (integer)$request->input('id');
            $mhsw = mahasiswa::find($id);

            if(failed($mhsw)){
                return response()->json(["message" => "failed"], 404);
            }
            $mhsw->save();

            DB::commit();
            return response()->json(["message" => "Success !!"], 200);

        }

        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
        }

    }